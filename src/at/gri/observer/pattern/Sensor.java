package at.gri.observer.pattern;

import java.util.ArrayList;
import java.util.List;

public class Sensor implements Observer {

	private List<Observable> Observables;

	public Sensor() {
		super();
		this.Observables = new ArrayList<Observable>();
	}

	public void addObserveable(Observable O1) {
		this.Observables.add(O1);
	}

	@Override
	public void InformAll() {
		// TODO Auto-generated method stub
		for (Observable o : this.Observables) {
			o.inform();
		}
	}

	@Override
	public void addObs(Observable Obs) {
		// TODO Auto-generated method stub

	}

}
