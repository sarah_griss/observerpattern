package at.gri.observer.pattern;

public interface Observable {
	public void inform();
}
