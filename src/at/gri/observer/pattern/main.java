package at.gri.observer.pattern;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Sensor s1 = new Sensor();
		Lantern l1 = new Lantern();
		Christmastreelight c1 = new Christmastreelight();
		
		s1.addObserveable(l1);
		s1.addObserveable(c1);
		
		s1.InformAll();
	}

}
